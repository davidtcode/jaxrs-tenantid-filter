package davidtcode.rest.resource;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import davidtcode.Main;
import static org.junit.Assert.*;

/**
 * @author David.Tegart
 */
public class FruitResourceTest {

	private HttpServer server;
	private WebTarget target;

	@Before
	public void setUp() throws Exception {

		server = Main.startServer();

		Client client = ClientBuilder.newClient();

		target = client.target(Main.BASE_URI);
	}

	@After
	public void tearDown() throws Exception {
		server.shutdownNow();
	}

	@Test
	public void testGetFruit() {

		verifyFruitForTenantId("tenantA", "apple");
		verifyFruitForTenantId("tenantB", "orange");
		verifyFruitForTenantId("someUnknownTenant", "Unknown");
	}

	private void verifyFruitForTenantId(String tenantId, String expectedFruit) {

		Response rs = target.path(tenantId + "/fruit").request().get();

		assertEquals(Status.OK.getStatusCode(), rs.getStatus());
		
		String tenantFruit = rs.readEntity(String.class);
		assertEquals(expectedFruit, tenantFruit);
	}
}