package davidtcode.tenant;

import org.apache.commons.lang3.StringUtils;

/**
 * @author David.Tegart
 */
public abstract class TenantContextUtil {

	private static final ThreadLocal<String> threadLocal = new InheritableThreadLocal<>();
	
	public static void setTenantId(String tenantId) {
		threadLocal.set(tenantId);
	}

	public static String getTenantId() {
		return threadLocal.get();
	}

	public static void unsetTenantId() {
		threadLocal.remove();
	}

	public static boolean isTenantIdSet() {
		return (StringUtils.isBlank(threadLocal.get()) ? false : true);
	}
}