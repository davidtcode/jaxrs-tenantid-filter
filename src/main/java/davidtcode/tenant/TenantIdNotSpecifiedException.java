package davidtcode.tenant;

/**
 * @author David.Tegart
 */
public class TenantIdNotSpecifiedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TenantIdNotSpecifiedException() {}

	public TenantIdNotSpecifiedException(String message) {
		super(message);
	}

	public TenantIdNotSpecifiedException(String message, Throwable t) {
		super(message, t);
	}
}