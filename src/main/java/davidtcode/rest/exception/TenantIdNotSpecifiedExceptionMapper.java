package davidtcode.rest.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import davidtcode.tenant.TenantIdNotSpecifiedException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author David.Tegart
 */
@Provider
public class TenantIdNotSpecifiedExceptionMapper implements
		ExceptionMapper<TenantIdNotSpecifiedException> {

	private static final Logger LOG = LoggerFactory
			.getLogger(TenantIdNotSpecifiedException.class);

	public TenantIdNotSpecifiedExceptionMapper() {}

	@Override
	public Response toResponse(TenantIdNotSpecifiedException ex) {

		LOG.error("A tenant Id exception has occured", ex);

		return Response.status(Response.Status.BAD_REQUEST)
				.type(MediaType.TEXT_PLAIN).entity(ex.getMessage()).build();
	}
}