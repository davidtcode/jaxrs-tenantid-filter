package davidtcode.rest.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import davidtcode.tenant.TenantContextUtil;

/**
 * @author David.Tegart
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
@Component
public class TenantContextResponseFilter implements ContainerResponseFilter {

	@Value("${log.mdc.tenantId}")
	private String mdcTenantIdName;
	
	@Override
	public void filter(ContainerRequestContext requestContext,
			ContainerResponseContext responseContext) throws IOException {

		// This thread may go now back into the pool, so ensure tenant id is wiped
		TenantContextUtil.unsetTenantId();

		MDC.remove(mdcTenantIdName);
	}
}