package davidtcode.rest.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.Priorities;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import davidtcode.tenant.TenantIdNotSpecifiedException;
import davidtcode.tenant.TenantContextUtil;

/**
 * @author David.Tegart
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
@Component
public class TenantContextRequestFilter implements ContainerRequestFilter {

	private static final Logger LOG = LoggerFactory
			.getLogger(TenantContextRequestFilter.class.getCanonicalName());

	@Value("${rest.param.tenantId}")
	private String tenantIdParamName;

	@Value("${log.mdc.tenantId}")
	private String mdcTenantIdName;
	
	public TenantContextRequestFilter() {}
	
	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {

		// This Thread may be from a thread pool, so ensure a clean slate
		TenantContextUtil.unsetTenantId(); 

		String tenantId = (requestContext.getUriInfo().getPathParameters() == null ?
				null : requestContext.getUriInfo().getPathParameters(true).getFirst(tenantIdParamName));
		
		if(StringUtils.isBlank(tenantId)) {
			throw new TenantIdNotSpecifiedException("The tenant id cannot be extracted");
		}
		
		TenantContextUtil.setTenantId(tenantId);

		MDC.put(mdcTenantIdName, tenantId); // Useful for tenant-demarcated logs
	}
}