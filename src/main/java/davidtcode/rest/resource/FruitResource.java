package davidtcode.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import davidtcode.dao.FruitDao;

/**
 * @author David.Tegart
 */
@Component
public class FruitResource {

	@Autowired
	private FruitDao fruitDao;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getFruit() {
		return fruitDao.getFruit();
	}
}