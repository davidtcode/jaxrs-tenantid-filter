package davidtcode.rest.resource;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author David.Tegart
 */
@Path("/{tenantId}")
public class RootResource {

	private static final Logger LOG = LoggerFactory
			.getLogger(RootResource.class.getCanonicalName());

	@Path("/fruit")
	public Class<FruitResource> getFruitResource(@PathParam("tenantId") String tenantId) {
		
		LOG.debug("Tenant |{}| is requesting the fruit sub-resource", tenantId);

		return FruitResource.class;
	}
}