package davidtcode.dao.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import davidtcode.dao.FruitDao;
import davidtcode.tenant.TenantContextUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * @author David.Tegart
 */
@Repository("fruitDao")
public class FruitDaoImpl implements FruitDao {

	private static final Logger LOG = LoggerFactory
			.getLogger(FruitDaoImpl.class.getCanonicalName());

	private Map<String, String> storage = new ConcurrentHashMap<>();
	{
		storage.put("tenantA", "apple");
		storage.put("tenantB", "orange");
	}
	
	@Override
	public String getFruit() {

		String fruit = storage.get(TenantContextUtil.getTenantId());
		
		LOG.trace("Fruit type for tenant |{}| is |{}|", 
				TenantContextUtil.getTenantId(), fruit);
		
		if(StringUtils.isBlank(fruit)) {
			return "Unknown";
		}
		
		return fruit;
	}
}