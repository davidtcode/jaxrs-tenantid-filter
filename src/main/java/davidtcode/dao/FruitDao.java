package davidtcode.dao;

/**
 * @author David.Tegart
 */
public interface FruitDao {

	public String getFruit();
}