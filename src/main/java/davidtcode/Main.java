package davidtcode;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import davidtcode.spring.SpringAnnotationConfiguration;

import java.io.IOException;
import java.net.URI;

/**
 * @author David.Tegart
 */
public class Main {

	public static final URI BASE_URI = URI.create("http://localhost:8888/rest/");

	public static void main(String[] args) throws IOException {
		
		HttpServer server = null;
		try {
			
			server = startServer(); // Start the Grizzly server

			System.out.println("Crtl-C to stop the http server ...");
			System.in.read();
		} finally {
			server.shutdownNow();
		}
	}

	public static HttpServer startServer() {

		// Create the jax-rs resource config
		ResourceConfig rc = new ResourceConfig()
				.packages("davidtcode.rest")
				.property("contextConfig", new AnnotationConfigApplicationContext(SpringAnnotationConfiguration.class));

		return GrizzlyHttpServerFactory // Start the http server
				.createHttpServer(BASE_URI, rc);
	}
}