### jaxrs-tenantid-filter

In a multi-tenanted application, one of the typical goals it to present to the tenant the *illusion* that they are using the application in isolation, that they have it to themselves as it were. An analogous goal from the software point of view, is to provide an *abstraction* (or an isolation) of the application's tenant-related logic. So instead of littering the code with such logic, it should exist in a layer abstracted away from the non-tenant specific application logic. Of course, at *some* places it won't and can't be abstract: e.g. usually at the persistence layer it is necessary to know what the tenant is because that information is needed to isolate one tenant's data from the next. The goal, more precisely, is provide as much as an abstraction as it is reasonable to do so. What is provided here is a simple demonstration, in a web application, of how to isolate such tenant-related logic.

This happens as follows. Firstly, the REST resource URI requires a tenant Id to be present as a path parameter, such as "/rest/{tenantId}/fruit". A Jax-rs filter (*TenantContextRequestFilter.java*) takes this tenant Id and binds it to the current thread. That tenant Id, though kept independent of most of the application logic, can be easily accessed through the use of a static method call: *TenantContextUtil.getTenantId()*. 

The example uses 2 tenants named tenantA and tenantB. Each is associated with a different type of fruit and each's call to the fruit REST resource should return that tenant's type. So:

* Clone the repository
* Change directory to base of the repository
* Run *mvn clean install*
* Run *mvn exec:java* (Starts up the Jersey Grizzly container and deploys the resource)
* Hit http://localhost:8888/rest/tenantA/fruit (Will return "apple")
* Hit http://localhost:8888/rest/tenantB/fruit (Will return "orange")

The code which handles those 2 requests is tenant-agnostic apart from (1) the filter and (2) the dao (*FruitDaoImpl.java*) where the tenant Id is retrieved and used to access the data corresponding to the given tenant. 

If more resources are added (in addition to *FruitResource.java*) we'd like to avoid having to add *`@Path("/{tenantId}/...")`* to each. We can indeed avoid this by using a *sub-resource locator* (see *RootResource.java*) which specifies that tenant Id path parameter and which also locates all the sub-resources, which in turn don't need to reference it. 

### Some limitations and notes

* The tenant Id doesn't need to be located in the path. It could be a header field or part of the domain name.
* The Dao in this example, for illustration purposes, simply stores the data for the 2 tenants in a Map. When it comes to storing and isolating multiple tenant data in a database, see https://msdn.microsoft.com/en-us/library/aa479086.aspx for an in-depth analysis. 
* The current approach works well in a *standard* web application, where each request is serviced by a single thread and where that request can be easily filtered or intercepted in such a way that the required information can be extracted, bound to the current thread, and used later. But it won't work so well if this single-thread-per-tenant-request model isn't available. 
* Similarly, functionality which occurs outside of this model needs to be handled slightly differently. For example, jobs executed on start-up or background tasks which aren't initiated by a user request. Here the tenant Id may need to be set manually, using *TenantContextUtil.setTenantId(...)*.
* The filter *TenantContextRequestFilter.java* is not a pre-matching filter. (See *javax.ws.rs.container.PreMatching*) This means that the REST resource has already been matched and chosen (based on path parameters, content-types etc.) by the time the filter has been called. The advantage of this is that the tenant Id has already been stripped from the URI and is made available in the *ContainerRequestContext* instance. This avoids needlessly parsing the URI twice and so aids performance. On the downside, the tenant Id isn't yet set when ContainerRequestContext is being executed and thus can't be accessed there.